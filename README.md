# slowa/cites

These are jsut some experiments in visualizing the research literature around
web archives.

## Wayback Machine

This data was generated with [etudier] searching for "Wayback Machine" and
gathering results with `--pages 10`. This means that etudier collected 10 pages
of results, and for each result it went and got up to ten pages of citing
articles.

  etudier --pages 10 https://scholar.google.com/scholar\?hl\=en\&as_sdt\=7%2C39\&q\=%22wayback+machine%22\&btnG\= --output wayback

It collected 2112 nodes (papers) with 2461 directed edges (citations) which
were saved in these two files:

- wayback.gexf 
- wayback.html

The GEXF file was loaded into Gephi to create a new Gephi
project:

* wayback.gephi

This project contains two workspaces:

1. *Workspace 1:* the initial graph, after running modularity detection to
   assign nodes to communities which were the assigned colors. The Force
   Network visualization was run. 

2. *Workspace 2*: the central grouping of citatoins in Workspace 1 was filtered into a new workspace and then modularity detection was run again. This graph has 1206 nodes and 1502 edges. PDF and PNG exports were created with labels only for nodes with a degree greater than 40. Which leaves a lot to be desired.

    * wayback-workspace2.pdf
    * wayback-workspace2.png

<a href="https://gitlab.com/slowa/cites/-/raw/main/wayback-workspace2.png"><img width="800" src="https://gitlab.com/slowa/cites/-/raw/main/wayback-workspace2.png"></a>

[etudier]: https://github.com/edsu/etudier
