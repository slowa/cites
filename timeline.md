# Timeline

It's hard to know what sort of granularity to use here. But these are some, 
albeit subjective, but significant milestones in the development of web
archives technology. These items have been chosen for what they indicate about
the use of web archives at particular points in time and location, and not to
comprehensively all developments.

1990: First web server at CERN

1994: Amazon and Yahoo founded

1996: Alexa and Internet Archive founded

1996: Pandora project started at the National Library of Australia

1996: Kulturarw3 project started at the National Library of Sweden

1998: Google founded

1998: OCLC CORC service launched for cataloging the web.

1999: Alexa sold to Amazon (for stock)

2000: LOCKSS prototype released

2000: Library of Congress and University of Michigan web archives created
      (using httrack)

2001: Wayback Machine launched

2002: Kahle's IIPC proposal

2002: Iceland and Norway begin legal deposit crawls

2003: IIPC Founded

2004: Heretrix software released

2005: OpenWayback development begun (git revision history)

2006: Archive-It created

2009: WARC published as ISO 28500:2009

2009: Hanzo Archives founded (out of a British Library project)

2013: pywb development began (git revision history)

2013: Perma.cc project started at Harvard.

2017: Internet Archive ignores robots.txt files

